from counter import comparisons, swaps

def insertion(elements):
    
    if len(elements) < 2:
        return elements
    
    for i in range(1, len(elements)):
    
        while elements[i - 1] > elements[i] and i > 0:
            comparisons.incr()
            
            elements[i], elements[i - 1] = elements[i - 1], elements[i]
            swaps.incr()
            
            i -= 1
            
        comparisons.incr()    
    return elements
        
# elements = [0, 4, 1, 2, 3, 1, 5, 2]
# print(insertion(elements))