from selection_sort import *
from benchmark import *
from report import *

bench(
    selection_sort_2, 
    sizes = [1000, 2000, 5000, 10000, 20000, 25000], 
    distribution = random_list_alldifferent
    ).add_formats([FormatCSV]).report().stdout()
    
count_ops(
    selection_sort_2,
    sizes=[1000, 2000, 5000, 10000, 20000],
    list_types = all_list_types,
    ops=[comparisons, swaps]
).add_formats([FormatCSV]).report().stdout()