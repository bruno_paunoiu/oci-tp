from selection_sort import *
from benchmark import bench
from report import *

bench(
    selection_sort_2, 
    sizes = [1000, 2000, 5000, 10000, 20000], 
    distribution=random_list_alldifferent
    ).add_formats([FormatCSV]).report().stdout()