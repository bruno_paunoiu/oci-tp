def insert(element, sequence):
 
    if sequence==[]:
        return [element]
    elif element<=sequence[0]:
        return [element] + sequence
    else:
        return [sequence[0]] + insert(element, sequence[1:len(sequence)])
 

 
def merge(subSequence1,subSequence2):
 
    if subSequence1==[]:
        return subSequence2
    elif subSequence2==[]:
        return subSequence1
    else:
        return merge(subSequence1[1:len(subSequence1)],insert(subSequence1[0], subSequence2))

 
def mergeSort(sequence):
    
    n = len(sequence)
    # definition de n
 
    if len(sequence)==0 or len(sequence)==1:
        return sequence
    else:
        return merge(mergeSort(sequence[0:n//2]),mergeSort(sequence[n//2:n]))
        # suppression du + 1 qui sautait un element de la liste
        
print(mergeSort([0, 4, 1, 2, 3, 1,5, 6, 8, 5, 15]))

