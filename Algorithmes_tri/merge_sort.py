from counter import comparisons, swaps

def merge_sort(elements):

    # ancrage de la récursion : lorsqu'il n'y a plus qu'un seul élément à trier,
    # il n'y a plus rien à faire
    if len(elements) > 1:

        # découpage en deux sous-listes de taille égale (plus ou moins 1 élément
        # pour les listes de taille impaire)
        mid = len(elements) // 2
        lefthalf = elements[:mid]
        righthalf = elements[mid:]

        # appels récursifs
        merge_sort(lefthalf)
        merge_sort(righthalf)

        # fusion des deux sous-listes ``lefthalf`` et ``righthalf``
        i, j, k = 0, 0, 0
        while i < len(lefthalf) and j < len(righthalf):
            comparisons.incr()
            comparisons.incr()
            
            if lefthalf[i] < righthalf[j]:
                comparisons.incr()
                
                elements[k]=lefthalf[i]
                swaps.incr()
                
                i=i+1
            else:
                comparisons.incr()
                elements[k]=righthalf[j]
                swaps.incr()
                
                j=j+1
            k=k+1
        comparisons.incr()

        while i < len(lefthalf):
            comparisons.incr()
            
            elements[k]=lefthalf[i]
            swaps.incr()
            
            i=i+1
            k=k+1
        comparisons.incr()

        while j < len(righthalf):
            comparisons.incr()
            
            elements[k]=righthalf[j]
            swaps.incr()
            
            j=j+1
            k=k+1
        comparisons.incr()

    return elements