from merge_sort import *
from benchmark import *
from report import *

bench(
    merge_sort, 
    sizes = [1000, 10000, 20000, 100000, 500000, 1000000], 
    distribution = random_list_alldifferent
    ).add_formats([FormatCSV]).report().stdout()
    
count_ops(
    merge_sort,
    sizes=[1000, 10000, 20000, 100000, 500000],
    list_types = all_list_types,
    ops=[comparisons, swaps]
).add_formats([FormatCSV]).report().stdout()