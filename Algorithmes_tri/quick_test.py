from quick_sort import *
from benchmark import *
from report import *

bench(
    quicksort, 
    sizes = [1000, 10000, 20000, 100000, 500000, 1000000], 
    distribution = random_list_alldifferent
    ).add_formats([FormatCSV]).report().stdout()
    
count_ops(
    quicksort,
    sizes=[100, 200, 400, 800],
    list_types = all_list_types,
    ops=[comparisons, swaps]
).add_formats([FormatCSV]).report().stdout()