from math import *
from copy import deepcopy

class Point(object):
    
    nbrPoints = 0

    def __init__(self, coord_x = 0, coord_y = 0):
        self.x = coord_x
        self.y = coord_y
        Point.nbrPoints += 1

    def __str__(self):
        return "({}, {})".format(self.x, self.y)
        
    def __repr__(self):
        return "Point({}, {})".format(self.x, self.y)
        
    def __add__(self, other):
        x_res = self.x + other.x
        y_res = self.y + other.y
        res = Point(x_res, y_res)
        return res
        
    def Distance(self, other):
        x_res = self.x - other.x
        y_res = self.y - other.y
        return sqrt(x_res ** 2 + y_res **2)
    
    def count():
        return Point.nbrPoints
      

class Rectangle(object):
    
    def __init__(self, corner = Point, width = float, heigth = float):
        self.A = corner
        self.B = Point(self.A.x + width, self.A.y - heigth)
        self.heigth = heigth
        self.width = width
        
    def area(self):
        return self.heigth * self.width
        
    def get_center(self):
        return Point((self.A.x + self.B.x) / 2, (self.A.y + self.B.y) / 2)
        
    def clone(self):
        clone = deepcopy(self)
        return clone
        
    def __mul__(self, n = int):
        A = Point(self.A.x - (self.width / 2) * (n - 1), self.A.y + (self.heigth / 2) * (n - 1))
        heigth = n * self.heigth
        width = n * self.width
        new = Rectangle(A, width, heigth)
        return new
    
class Square(Rectangle):
    
    def __init__(self, corner = Point, side = float):
        Rectangle.__init__(self, corner, side, side)
        
    def __repr__(self):
        return "Square({})".format(self.length)
    
    
class Circle(object):
    
    def __init__(self, center = Point, radius = float):
        self.center = center
        self.radius = radius
        
    def get_center(self):
        return self.center
        
    def area(self):
        return pi * (self.radius ** 2)
        
    def __contains__(self, point = Point):
        return self.center.Distance(point) <= self.radius
        
        

corner = Point(12, 27)
rect = Rectangle(corner, 50, 36)
copie = rect.clone()
print(rect == copie)
print(rect.area())
print(rect.get_center())
cornerr = Point(10, 5)
square = Square(cornerr, side = 10)
print(square.area())
print(square.get_center())
center = Point(0, 0)
c = Circle(center, radius = 10)
print(c.area())
print(c.get_center())
p1 = Point(3, 4)
p2 = Point(20, 0)
print(p1 in c)
print(p2 in c)
newrect = rect * 4
print(newrect.area())
print(newrect.get_center())

