from main import *

debug = True

def char_freq(text):
    
    dico = {}
    
    for word in to_words(text):
        
        if word in dico:
            dico[word] += 1
        
        else:
            dico[word] = 1
            
    return dico
    



def histogramme(freq):
    
    sorted_words = sorted(freq.items(), key=lambda x: x[1])
    sorted_words = sorted_words[::-1]
    
    print('Mots\t\tFrequences\tHistogramme\n' + ('=' * 50))
    
    for word, number in words_list_of_tuple:
        if len(word) > 7:
            print(word + '\t' + str(number) + '\t\t' + ('*' * number))
        else:
            print(word + '\t\t' + str(number) + '\t\t' + ('*' * number))
        
        
if debug: histogramme(char_freq('le cours de informatique est un cours 7lettre 8lettres'))
