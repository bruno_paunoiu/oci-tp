punctuation = ',;.:?!"\''

    
def to_words(text):
    words = []
    
    for i in text:
        if i in punctuation :
            text = text.replace(i, ' ')
    words = list(text.split())
    
    return words
    
def max_word_len(sentence):
    words = to_words(sentence)
    
    word_count = (0,0)
    
    for i in range(len(words)):
        if len(words[i]) > word_count[1]:
            word_count = (i, len(words[i]))
            
    return word_count
    
def make_unique(elements):
    unique_elements = []
    
    for i in elements:
        if i not in unique_elements:
            unique_elements.append(i)
            
    return unique_elements
    
def swap_key_values(dictionary):
    
    try:
        new_dict = dict((v,k) for k,v in dictionary.items())
        
    except:
        print ('Error')
        
    return new_dict
    
