class Recipient(object):

    def __init__(self, contenance, no):
       self.no = no
       self.contenance = contenance
       self.volume = 0

    def vider(self):
       self.volume = 0

    def transferer(self, autre):
        quantite_transferee = min(autre.contenance - autre.volume, self.volume)
        autre.volume += quantite_transferee
        self.volume -= quantite_transferee

    def remplir(self):
       self.volume = self.contenance

    def __str__(self):
        return "Récipient no {}, Contenance : {}, Volume : {}".format(self.no,
                                                                      self.contenance,
                                                                      self.volume)

    def __repr__(self):
        return str(self)
        
r1 = Recipient(no = 1, contenance = 5)
r2 = Recipient(no = 2, contenance = 3)

r1.remplir()
r2.transferer(r1)
r2.vider()
r2.transferer(r1)
r1.remplir()
r2.transferer(r1)

assert(r1.volume == 4)